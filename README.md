# Workstation UBUNTU
Este repositório contém scripts para automatizar e acelerar o fluxo de trabalho e preparação para a minha máquina. 

> **_Descrição_** : 
> Esses scripts estão relacionados com a versão principal 20+, para outras distribuições que você precisará para adaptá-lo 

## Prepare estação de trabalho > Leia o arquivo 'ubuntu.yml' antes de aplicar e certifique-se de entender tudo o que será feito. 
1. Instale Ansible '''bash sudo apt atualização && sudo apt instalar ansible unzip git -y ``` 
2. Clone este repositório '''bash https://gitlab.com/lauromello/works_ubuntu.git  ``` 
3. Aplique a configuração '''bash ansible-playbook tools/ubuntu.yml --ask-become-pass ``` 
>Tipar sua senha quando solicitado a dar permissões raiz para algumas ações.